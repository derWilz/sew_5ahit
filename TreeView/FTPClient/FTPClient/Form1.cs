﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace FTPClient
{
    public partial class Form1 : Form
    {
        XElement elem;
        public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("127.0.0.1", 2055);
            try
            {
                Stream stream = client.GetStream();

                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);

                writer.AutoFlush = true;

                writer.WriteLine(textBox1.Text);
                TreeNode node = new TreeNode();
                elem = XElement.Parse(reader.ReadToEnd());

                node = ToTreeNode(elem);
                treeView1.Nodes.Add(node);
                client.Close();
            }
            catch (Exception)
            {
            }
        }

        public static TreeNode ToTreeNode(XElement elem)
        {
            return new TreeNode(elem.Attribute("Name").Value,
                                    elem.Elements().Select(e => ToTreeNode(e)).ToArray());
        }
    }
}
